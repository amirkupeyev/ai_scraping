from fastapi import FastAPI, HTTPException 
from typing import List
import pandas as pd
from backend_api.ML import AI_Scraper  
from starlette.responses import RedirectResponse
from pydantic import BaseModel
from backend_api.ML import price_predict
import json

app = FastAPI()

class TickerList(BaseModel):
    tickers: List[str] 

class InputData(BaseModel):
    input_string: str


@app.get("/")
async def root():
    return RedirectResponse(url="http://localhost:8501")




@app.post("/get_ticker_info/")
async def get_ticker_info(tickers: TickerList):
    try:
        df = AI_Scraper.main(tickers.tickers)
        df_json = df.to_json(orient="records")
        return {"data": df_json}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e)) 


@app.post("/download_and_predict/")
async def download(ticker :InputData ):
    try:
        predicted = price_predict.main(ticker.input_string) 
        return{"result" : predicted} 
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e)) 
    




