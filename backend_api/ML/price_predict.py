from backend_api.ML.shared import model1 , encoder , scaler 
from sklearn.preprocessing import StandardScaler, OneHotEncoder
import joblib
import numpy as np 
import requests
from bs4 import BeautifulSoup 
from urllib.parse import urljoin
import requests
from bs4 import BeautifulSoup 
from backend_api.ML.shared import headers
import pandas as pd


def main(ticker):  
    url = 'https://finance.yahoo.com/quote/{}/history?p={}'.format(ticker, ticker)
    response = requests.get(url, headers=headers, verify=False)
    soup = BeautifulSoup(response.text, 'html.parser')
    div_id = 'YDC-Col1'
    specific_div = soup.find('div', id=div_id)

    unique_hrefs = set()
    if specific_div:
        atags = specific_div.find_all('a')
        for link in atags:
            if 'href' in link.attrs:
                unique_hrefs.add(link['href'])

    csv_link = ''
    for href in unique_hrefs:
        csv_link = href
        
    if not csv_link.startswith(('http://', 'https://')):
        csv_link = urljoin(url, csv_link) 

    response = requests.get(csv_link, headers=headers)

    if response.status_code == 200:
        with open('downloaded_file.csv', 'wb') as file:
            file.write(response.content)
        print("CSV file downloaded successfully.")
    else:
        print(f"Failed to download CSV file. Status code: {response.status_code}")

    file_path = 'downloaded_file.csv' 
    df5 = pd.read_csv(file_path) 
    print('working')
    df5['Ticker'] = ticker
    df5 = df5[df5.shape[0]-31:] 
    company_encoded = encoder.transform(df5[['Ticker']])
    closing_prices = scaler.transform(df5[['Close']].values) 
    processed_data = np.hstack((company_encoded, closing_prices))

    def create_sequences(data, N, num_company_columns):
        X, y = [], []
        for i in range(len(data) - N):
            X.append(data[i:i+N, :num_company_columns+1])
            y.append(data[i+N, -1])
        return np.array(X), np.array(y) 

    
    N = 30 
    num_company_columns = company_encoded.shape[1]

    X, y = create_sequences(processed_data, N, num_company_columns) 
    c = model1.predict(X) 
    c= scaler.inverse_transform(c.reshape(-1, 1))
    c=c[0][0]
    return c