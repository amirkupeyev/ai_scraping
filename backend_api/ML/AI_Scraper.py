import requests
from bs4 import BeautifulSoup
from transformers import PegasusTokenizer , PegasusForConditionalGeneration 
import socket 
from transformers import pipeline
import pandas as pd
from backend_api.ML.shared import headers , model_name , tokenizer , model

def final_url(url):
    while True:
        response = requests.get(url, allow_redirects=False)
        if response.status_code in range(300, 400):
            url = response.headers['Location']
        else:
            return url 


def search_for_stock_news_urls(ticker):
    search_url = "https://news.google.com/search?q=cnbc+finance+{}&hl=en-US&gl=US&ceid=US:en".format(ticker)
    r = requests.get(search_url,headers=headers,verify=False)
    soup = BeautifulSoup(r.text, 'html.parser')
    atags = soup.find_all('a')
    unique_hrefs = set()
    for link in atags:
        if 'href' in link.attrs:
            unique_hrefs.add(link['href'])

    return list(unique_hrefs)

def modify_links(strings):
    """Deletes the first character from each string in a list."""

    modified_strings = [string[1:] for string in strings] 
    modified_strings = [string for string in modified_strings if "/articles" in string]
    modified_strings = ["https://news.google.com" + string for string in modified_strings]
    return modified_strings 

def get_urls(ticker):
    raw_urls = []
    links = search_for_stock_news_urls(ticker)  # Attempt to fetch links
    modified_links = modify_links(links)
    raw_urls= modified_links
    #except socket.gaierror as e:
        #if e.errno == 11001:  # Check for specific error code
            #print(f"Error fetching links for {ticker}: {e}. Skipping to next ticker.")
       # else:
           # raise  # Re-raise other errors for visibility
    return raw_urls 

import requests
from bs4 import BeautifulSoup
import socket

def scrape_and_process(URLs):
    ARTICLES = []
    for url in URLs: 
        if (len(ARTICLES)>19):
            return ARTICLES
        else:
            try:
                f_url = final_url(url)
                r = requests.get(f_url,headers=headers,verify=False)
                soup = BeautifulSoup(r.text, 'html.parser')
                paragraphs = soup.find_all('p')
                paragraphs = paragraphs[111:len(paragraphs)-6]
                text = [paragraph.text for paragraph in paragraphs]
                if (len(text)<7):
                    continue
                else:
                    words = ' '.join(text).split(' ')[:350]
                    ARTICLE = ' '.join(words)
                    ARTICLES.append(ARTICLE)

            except requests.ConnectionError as e:
                print(f"Connection error occurred for {url}: {e}. Skipping to next URL.")

            except socket.gaierror as e:
                if e.errno == 11001:
                    print(f"Network address-related error occurred for {url}: {e}. Skipping to next URL.")
                else:
                    print(f"Socket error occurred for {url}: {e}. Skipping to next URL.")

            except requests.RequestException as e:
                print(f"Error occurred for {url}: {e}. Skipping to next URL.")
    return ARTICLES 


def get_articles(monitored_tickers):
    articles = {ticker:scrape_and_process(get_urls(ticker)) for ticker in monitored_tickers}
    return articles
  
def summarize(articles):
    summaries = []
    for article in articles:
        input_ids = tokenizer.encode(article, return_tensors='pt')
        output = model.generate(input_ids, max_length=55, num_beams=5, early_stopping=True)
        summary = tokenizer.decode(output[0], skip_special_tokens=True)
        summaries.append(summary)
    return summaries
        

def create_output_array(summaries, scores , monitored_tickers):
    output = []
    for ticker in monitored_tickers:
        for counter in range(len(summaries[ticker])):
            output_this = [
                ticker,
                summaries[ticker][counter],
                scores[ticker][counter]['label'],
                scores[ticker][counter]['score'],
            ]
            output.append(output_this)
    return output 

def get_company_news(monitored_tickers):
    sentiment = pipeline('sentiment-analysis')
    articles = get_articles(monitored_tickers) 
    summaries = {ticker:summarize(articles[ticker]) for ticker in monitored_tickers}
    scores = {ticker:sentiment(summaries[ticker]) for ticker in monitored_tickers}
    return create_output_array(summaries, scores,monitored_tickers) , articles 

       
def main(monitored_tickers):
    print(monitored_tickers)
    print("main function launched")
    final_output = get_company_news(monitored_tickers)
    articles = final_output[1]
    news_info = final_output[0] 
    news_info.insert(0 , ['Company' ,'Summary' , 'Sentiment' , 'Score']) 
    df1 = pd.DataFrame(news_info)
    new_header = df1.iloc[0]  
    df1 = df1[1:]  
    df1.columns = new_header  
    df1.reset_index(drop=True, inplace=True)

    return df1
    

    

