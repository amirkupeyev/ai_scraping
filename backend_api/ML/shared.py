from transformers import PegasusTokenizer , PegasusForConditionalGeneration 
from tensorflow.keras.models import load_model 
from sklearn.preprocessing import StandardScaler, OneHotEncoder 
import joblib


headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36'}
model_name = "human-centered-summarization/financial-summarization-pegasus"
tokenizer = PegasusTokenizer.from_pretrained(model_name)
model = PegasusForConditionalGeneration.from_pretrained(model_name) 

model1 = load_model('my_lstm_model.h5')

encoder = joblib.load('encoder.joblib')

scaler = joblib.load('scaler.joblib')