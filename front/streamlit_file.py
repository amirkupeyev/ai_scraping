import streamlit as st
import requests
import pandas as pd

st.title("Ticker Information Viewer")

tickers = st.text_input("Enter tickers separated by commas")
if st.button("Get Ticker Info"):
    response = requests.post('http://127.0.0.1:8000/get_ticker_info/', json={"tickers": tickers.split(",")})
    print(response.status_code, response.text)
    
    if response.status_code == 200:
        df_data = response.json()["data"]
        df = pd.read_json(df_data)
        st.dataframe(df)
    else:
        st.error("Error in processing request") 

st.subheader("Predict Stock Price")
additional_input = st.text_input("Enter a ticker here")
if st.button("Submit here"):
    additional_response = requests.post('http://127.0.0.1:8000/download_and_predict/', json={"input_string": additional_input})
    
    if additional_response.status_code == 200:
        additional_data = additional_response.json()  
        st.write(additional_data)  
    else:
        st.error("Error in processing additional request")
